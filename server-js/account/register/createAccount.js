const mysqlPool = require('../../mysql/mysqlPool')
const checkIfAccountAlreadyExists = require('../validation/checkIfAccountAlreadyExists')
const uuid = require('uuid/v1')

module.exports = (username, password, email, res) => {
  if(username && password && email) {
    const accountDefaultStats = {
      adminLevel: 0,
      session_id: uuid()
    }

    const createNewAccountSql = 'INSERT INTO accounts (username, email, password, session, session_id, admin_level) VALUES (?, ?, ?, ?, ?, ?)'
    const values = [
      username,
      email,
      password,
      accountDefaultStats.session_id,
      0,
      accountDefaultStats.adminLevel,
    ]


    checkIfAccountAlreadyExists(username, email).then(accountExists => {
      console.log('Does account exist ====> ' + accountExists)
      if(!accountExists) {
        mysqlPool.query(createNewAccountSql, values, (error, results, fields) => {
          if (error) throw error
          console.log("New account created: "+username)
          res.send(accountDefaultStats.session_id)
          return results
        })
        console.log('Attempted to create a new user account for: '+username)
      } else {
        console.log('Account already exists for: '+username)
        res.send(false)
        return false
      }
    })
  }
}
