const mysqlPool = require('./mysqlPool')

module.exports = (sqlQuery, customErrorMessage, callback) => {
  mysqlPool.query(sqlQuery, (error, results, fields)=>{
    if(error) {console.log(customErrorMessage)}
    if(callback) {
      callback(error, results, fields)
    }
    return results
  })
}
