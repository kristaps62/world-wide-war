const treasureListData = [
    {
        name: 'Small Treasure',
        tier: 0,
        price: 100,
        health: 900,
        spawnChance: 100
    },
    {
        name: 'Medium Treasure',
        tier: 1,
        price: 300,
        health: 1200,
        spawnChance: 50
    },
    {
        name: 'Large Treasure',
        tier: 2,
        price: 1000,
        health: 6000,
        spawnChance: 10
    }
]

const treausreConfig = {
    maxTreasuresSpawned: 100,
    minTreasuresSpawned: 10
}

function getRandomNumber(min, max) {
    return Math.floor(Math.random() * (max - min) ) + min
}

const getRandomTreasure = () => {
    const treasureChance = getRandomNumber(0, 100)
    let treasureData = ''
    treasureListData.map( treasure => {
        if(treasure.spawnChance > treasureChance) {
            treasureData = treasure
        }
    })
    return treasureData
 }

 module.exports = () => {
     const treasureAmmountInWorld = getRandomNumber(treausreConfig.minTreasuresSpawned, treausreConfig.maxTreasuresSpawned)
     const treasureData = new Array(treasureAmmountInWorld).fill({customType: 'Treasure'})
     treasureData.map( (treasure, index) => {
         const randomTreasure = getRandomTreasure()
         treasureData[index] = randomTreasure
         treasureData[index].x = getRandomNumber(500, 3000)
         treasureData[index].y = getRandomNumber(500, 3000)
         treasureData[index].customType = 'Treasure'
     })
     return treasureData
 }


