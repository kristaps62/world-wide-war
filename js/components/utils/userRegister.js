import validateEmail from './validation/emailValidation'
import validateUsername from './validation/usernameValidation'
import validatePassword from './validation/passwordValidation'
import { handleServerRequest } from './utils.js'

export default (registerData, callback) => {
    const { email, username, password, password2 } = registerData
    if(validateEmail(email) && validateUsername(username) && validatePassword(password, password2)) {
      console.log('validation passed')
      handleServerRequest({username, email, password}, 'register',callback)
    }
  }
