import defaultUrl from './config/url.js'

export const handleServerRequest = (data, apiPath ,callback) => {
return new Promise( (resolve, reject) => {
    fetch(defaultUrl+apiPath, {
      method: "POST",
      mode: "cors",
      cache: "no-cache",
      credentials: "same-origin",
      headers: {
          "Content-Type": "application/json",
      },
      redirect: "follow",
      referrer: "no-referrer",
      body: JSON.stringify(data),
    })
    .then( res => res.json())
    .then( data => resolve(callback ? callback(data) : data))
  })
}

export const getRandomNumber = (min, max) => {
  return Math.floor(Math.random() * (max - min) ) + min
}
