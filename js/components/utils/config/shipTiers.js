const shipTiers = [
    {
        tier: 0,
        shipName: 'Floop',
        health: 1000,
        broadSideRegen: 3,
        healthRegen: 2,
        leftBroadside: {
          key: 'left',
          stamina: 1000,
          maxStamina: 1000,
          cannonCount: 1
        },
        rightBroadSide: {
          key: 'right',
          stamina: 1000,
          maxStamina: 1000,
          cannonCount: 1
        },
        shotExhaustion: 999,
        shotDamage: 350,
        mass: 50000
    }
]

export default shipTiers