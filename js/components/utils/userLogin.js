import socket from './config/socket'
import validateEmail from './validation/emailValidation'
import validateUsername from './validation/usernameValidation'
import validatePassword from './validation/passwordValidation'
import { handleServerRequest } from './utils.js'

export default (loginData, setLoginError, callback) => {
    const { email, password } = loginData
    if(validateEmail(email)) {
      handleServerRequest({email, password}, 'login',callback)
    } else {
      setLoginError()
    }
  }
