import Phaser from 'phaser'

const Bullet = new Phaser.Class({

    Extends: Phaser.GameObjects.Image,

    initialize:

    function Bullet (scene)
    {
        Phaser.GameObjects.Image.call(this, scene, 0, 0, 'bullet');
        this.born = 0;
        this.direction = 0;
        this.x = 0;
        this.y = 0;
        this.setSize(15, 15, true);
        this.customType = "Bullet";
    },

    fire: function (shooter, angle)
    {
        this.setPosition(shooter.x, shooter.y);
        this.angle = angle;
        this.setMass(100)
        this.thrust(10)

        this.rotation = shooter.rotation; 
        this.born = 0;
    },

    update: function (time, delta)
    {
        this.born += delta;

        if (this.born > 400)
        {
            this.setActive(false);
            this.setVisible(false);
            this.scene.player.customSounds.emptyWaterSplash.play()
            this.destroy()
        }
    }

});

export default Bullet