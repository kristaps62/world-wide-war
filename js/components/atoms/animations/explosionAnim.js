import Phaser from 'phaser'

const ExplosionAnim = new Phaser.Class({

    Extends: Phaser.GameObjects.Image,

    initialize:

    function ExplosionAnim (scene, targetPosition)
    {
        scene.anims.create({
            key: 'explode',
            frames: scene.anims.generateFrameNumbers('explosionAnim', { start: 0, end: 54, first: 54 }),
            frameRate: 24
        })
        this.sprite = scene.add.sprite(targetPosition.position.x, targetPosition.position.y, 'explosionAnim')
        this.born = 0;
        this.x = targetPosition.position.x;
        this.y = targetPosition.position.y;
        this.sprite.anims.play('explode')
        setTimeout((explosionAnim)=>{
            explosionAnim.sprite.destroy()
        }, 2000, this)
    },
});

export default ExplosionAnim