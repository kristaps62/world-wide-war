import staminaHandler from '../statHandlers/stamina'

const sprint = (game, controls, mainCamera, player) => {
    if(controls.shift.isDown && player.stats.stamina > 3 && player.stats.staminaExhausted === false) {
        if(mainCamera.zoom < 1.2) {
            mainCamera.zoom+=0.02
        }
        player.runningSpeed = 5
        player.stats.stamina = staminaHandler(game, player.stats, false, 10)
        if(player.stats.stamina < 5) {
            player.stats.staminaExhausted = true
        }
    } else {
        if(mainCamera.zoom > 1) {
            mainCamera.zoom-=0.02
        }
        player.runningSpeed = 2
        player.stats.stamina = staminaHandler(game, player.stats, 5, false)
        if(player.stats.stamina > 300) {
            player.stats.staminaExhausted = false
        }
    }
}

export default sprint