const movementHandler = (controls, player, layer) => {
    if(controls.up.isDown && player.y > 30) {
        player.thrust(20)
    }
    if(controls.right.isDown && player.x < layer.width-30) {
        player.setRotation(player.rotation+=.01)
        player.thrustRight(5)
    }
    if(controls.left.isDown && player.x > 30) {
        player.setRotation(player.rotation-=.01)
        player.thrustLeft(5)
    }
}

export default movementHandler