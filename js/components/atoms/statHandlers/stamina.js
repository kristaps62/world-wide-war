const staminaHandler = (game, playerStats, newStatValues)=>{
    let { stamina, maxStamina }  = playerStats

    newStatValues.add ? game.events.emit('gainStamina', {side: newStatValues.side, ammount: newStatValues.ammount}) : false
    newStatValues.remove ? game.events.emit('spendStamina', {side: newStatValues.side, ammount: newStatValues.ammount}) : false
    var newStamina = stamina

    if(newStatValues.add) {
        newStamina+=newStatValues.ammount

        if(newStamina > maxStamina) {
            newStamina = maxStamina
        }
    }

    if(newStatValues.remove) {
        newStamina-=newStatValues.ammount

        if(newStamina < 0) {
            newStamina = 0
        }
    }
    return newStamina
}

export default staminaHandler