import Phaser from 'phaser'

const TreasureGenerator = new Phaser.Class({

    Extends: Phaser.GameObjects.Image,

    initialize:

    function TreasureGenerator (scene)
    {
        Phaser.GameObjects.Image.call(this, scene, 0, 0, 'smallChest');
    },

    updateTreasureData: function (treasure, id) {
        this.health = treasure.health
        this.price = treasure.price
        this.x = treasure.x
        this.y = treasure.y
        this.treasureType = treasure.name
        this.indexID = id
        this.customType = "treasure"
    },

    dealDamage: function (damage, damageDealerSocketId, socket)
    {
        this.health -= damage
        this.lastDamageDealer = damageDealerSocketId
        if(this.health < 0) {
            socket.emit('treasureDestroyed', JSON.stringify({
                playerSocketId: damageDealerSocketId,
                treasureReward: this.price
            }))

            this.destroy()
        }
    },

    update: function () {
        
    }

});

export default TreasureGenerator