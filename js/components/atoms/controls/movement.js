const controls = (input) => {
    return {
        up: input.keyboard.addKey('W'),
        down: input.keyboard.addKey('S'),
        left: input.keyboard.addKey('A'),
        right: input.keyboard.addKey('D'),
        shift: input.keyboard.addKey('SHIFT')
      }
}

export default controls