import Phaser from 'phaser'

class statBar {

    constructor (scene, x, y, goodStandingColor, badStandingColor, minStatRequirement)
    {
        this.bar = new Phaser.GameObjects.Graphics(scene);

        this.x = x;
        this.y = y;
        this.minStatRequirement = minStatRequirement;
        this.goodStandingColor = goodStandingColor;
        this.badStandingColor = badStandingColor;
        this.value = 1000;
        this.p = 76 / 1000;

        this.draw();

        scene.add.existing(this.bar);
    }

    decrease (amount)
    {
        this.value -= amount;

        if (this.value < 0)
        {
            this.value = 0;
        }

        this.draw();

        return (this.value === 0);
    }

    increase (amount){
        this.value += amount

        if(this.value > 1000) {
            this.value = 1000
        }

        this.draw()

        return (this.value === 1000)
    }

    draw ()
    {
        this.bar.clear();

        //  BG
        this.bar.fillStyle(0x000000);
        this.bar.fillRect(this.x, this.y, 80, 16);

        //  Health

        this.bar.fillStyle(0xffffff);
        this.bar.fillRect(this.x + 2, this.y + 2, 76, 12);

        if (this.value < this.minStatRequirement)
        {
            this.bar.fillStyle(this.badStandingColor ? this.badStandingColor : 0xff0000);
        }
        else
        {
            this.bar.fillStyle(this.goodStandingColor ? this.goodStandingColor : 0x00ff00);
        }

        var d = Math.floor(this.p * this.value);

        this.bar.fillRect(this.x + 2, this.y + 2, d, 12);
    }

}

export default statBar