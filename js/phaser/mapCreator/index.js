function createArray(dimensions) {
    var array = [];

    for (var i = 0; i < dimensions[0]; ++i) {
        array.push(dimensions.length == 1 ? 0 : createArray(dimensions.slice(1)));
    }

    return array;
}
let level = createArray([1024,1024])

const updateLevelTile = (x, y, tileId) => {level[x][y] = tileId}

function minMaxRandom(minValue, maxValue) {
  return Math.round(Math.random() * (maxValue - minValue) + minValue)
}

for (var i = 0; i < 50000; ++i) {
  updateLevelTile(minMaxRandom(0, 800), minMaxRandom(0, 800), 1)
}

export default level
