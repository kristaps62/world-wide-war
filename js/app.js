import React, { Component } from "react";
import ReactDOM from "react-dom";
import LoginPage from './containers/loginPage/index'
import MainMenu from './containers/mainMenu/index'
import styles from './app.css'

class App extends Component {
  constructor() {
    super();
    this.state = {
      loginToken: ''
    };
  }

  componentDidMount() {
    const loginToken = localStorage.getItem('exilelvLoginTokenStorage')
    localStorage && loginToken ? this.setState({loginToken}) : this.setState({loginToken: false})
  }

  render() {

    return (
      <div className={styles.wrapper}>
        {this.state.loginToken ? <MainMenu changeLoginToken={(newLoginToken)=>{this.setState({loginToken: newLoginToken})}} /> :  <LoginPage changeLoginToken={(newLoginToken)=>{this.setState({loginToken: newLoginToken})}} />}
      </div>
    )
  }
}
export default App;
