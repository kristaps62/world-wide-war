import statBar from '../../components/molecules/barCreator/statBar'

const UIOverlay = new Phaser.Class({

    Extends: Phaser.Scene,

    initialize:

    function UIOverlay ()
    {
        Phaser.Scene.call(this, { key: 'UIOverlay', active: true });
    },

    preload: function () {
        this.load.image("leftBroadSide", "../../../../assets/leftBroadSide.png");
        this.load.image("rightBroadSide", "../../../../assets/rightBroadSide.png");
        this.load.image("hpIcon", "../../../../assets/hpIcon.png");
    },

    create: function ()
    {
        this.minStaminaStep = {
            left: 500,
            right: 500
        }
        this.game.events.on('spendStamina', function (spentStamina) {
            if(spentStamina.side === 'left') {
                this.leftStaminaBar.decrease(spentStamina.ammount)
            } else {
                this.rightStaminaBar.decrease(spentStamina.ammount)
            }
        }, this)

        this.game.events.on('gainStamina', function (gainedStamina) {
            if(gainedStamina.side === 'left') {
                this.leftStaminaBar.increase(gainedStamina.ammount)
            } else {
                this.rightStaminaBar.increase(gainedStamina.ammount)
            }
        }, this)
        
        this.minStaminaStep = {
            left: 999,
            right: 999
        }
        
        this.leftStaminaBar = new statBar(this, 100, this.cameras.main.height-130, 0xDBC61C, 0xDE480B, this.minStaminaStep.left)
        this.leftStaminaIcon = this.add.sprite(85, this.cameras.main.height-127, 'leftBroadSide')

        this.rightStaminaBar = new statBar(this, 100, this.cameras.main.height-100, 0xDBC61C, 0xDE480B, this.minStaminaStep.right)
        this.rightStaminaIcon = this.add.sprite(85, this.cameras.main.height-95, 'rightBroadSide')
    },
});

export default UIOverlay