import movementHandler from '../../components/atoms/handleMovement/movement'
import ExplosionAnim from '../../components/atoms/animations/explosionAnim'
import Bullet from '../../components/atoms/bullets/bullet'
import staminaHandler from '../../components/atoms/statHandlers/stamina'
import socket from '../../components/utils/config/socket'
import controlCreation from '../../components/atoms/controls/movement'
import shipTiers from '../../components/utils/config/shipTiers'
import { getRandomNumber } from '../../components/utils/utils'
import TreasureGenerator from '../../components/atoms/treasures/treasureGenerator'

const GameScreen = new Phaser.Class({

    Extends: Phaser.Scene,

    initialize:

    function GameScreen ()
    {
        Phaser.Scene.call(this, { key: 'GameScreen', active: true });
    },

    preload: function () {
        this.load.image("player", "../../../../assets/ship.png");
        this.load.json('shipCollision', '../../../../assets/ship-collider.json')
        this.load.image("weapon", "../../../../assets/weapon.png");
        this.load.image("bullet", "../../../../assets/bullet.png");
        this.load.image("cannon", "../../../../assets/cannon.png");
        this.load.image('smallChest', "../../../../assets/smallChest.png");
        this.load.audio("cannonShot", "../../../../assets/CannonShot.mp3");
        this.load.audio("cannonExplosion", "../../../../assets/cannonExplosion.wav");
        this.load.audio("emptyWaterSplash", "../../../../assets/emptyWaterSplash.wav");
        this.load.spritesheet('explosionAnim', '../../../../assets/explosionAnim.png', { frameWidth: 64, frameHeight: 64, endFrame: 54 });
        this.load.tilemapTiledJSON("map", "../assets/grasslandsMap.json");
        this.load.image("tileset", "../assets/base_out_atlas.png")
      },

    create: function () {

      const map = this.make.tilemap({key: 'map'});
      const tileset = map.addTilesetImage("base_out_atlas", 'tileset');
      this.layer = map.createStaticLayer("ground", tileset, 0, 0)
      this.itemLayer = map.createDynamicLayer("items", tileset, 0, 0);
      this.matter.world.autoUpdate = false

      this.layer.setCollisionByProperty({ collides: true });
      this.itemLayer.setCollisionByProperty({ collides: true });

      this.matter.world.convertTilemapLayer(this.layer);
      this.matter.world.convertTilemapLayer(this.itemLayer);

      this.matter.world.setBounds(0, 0, this.layer.width, this.layer.height)
      this.matter.world.customType = "WorldCollisionBox"

      this.player = this.add.container(getRandomNumber(600,this.layer.width), getRandomNumber(600,this.layer.height))
      this.player.anchor = 0.5
      this.player.setSize(64,64)
      this.player.collisionBox = this.cache.json.get('shipCollision')
      this.matter.add.gameObject(this.player, { shape: this.player.collisionBox.ship})

      this.bullets = this.add.group({ classType: Bullet, runChildUpdate: true })
      this.treasureList = this.add.group({ classType: TreasureGenerator, runChildUpdate: true })

      const playerTexture = this.add.sprite(0, 0, 'player')
      const playerCannonLeft = this.add.sprite(-10, -30, 'cannon')
      const playerCannonRight = this.add.sprite(-10, 30, 'cannon')
      playerCannonRight.scaleY = -1
      this.player.add(playerCannonLeft)
      this.player.add(playerCannonRight)
      this.player.add(playerTexture)
      this.player.stats = shipTiers[0]
      this.player.socketId = socket.id
      this.player.customType = 'PlayerShip'
      this.player.setMass(this.player.stats.mass)

      socket.emit('newPlayerReady', {
        socketId: this.player.socketId,
        x: this.player.x,
        y: this.player.y,
        rotation: this.player.rotation,
        stats: this.player.stats,
        mass: this.player.mass,
        anchor: this.player.anchor,
      })

      setInterval(({socket, player})=> {
        const dataToSendOut = {
          socketId: player.socketId,
          x: player.x,
          y: player.y,
          rotation: player.rotation,
          stats: player.stats,
          mass: player.mass,
          anchor: player.anchor,
        }
        socket.emit('playerUpdated', JSON.stringify(dataToSendOut))
      }, 5, {socket, player: this.player, matter: this.matter})

      this.playerList = []

      socket.on('worldTreasureData', (treasureWorldData)=>{
        JSON.parse(treasureWorldData).map( (treasure, index) => {
          let newTreasure = this.treasureList.get().setActive(true).setVisible(true)
          if(newTreasure) {
            newTreasure.updateTreasureData(treasure, index)
            newTreasure = this.matter.add.gameObject(newTreasure, { isSensor: true })
          }
        })
      })

      socket.on('playerDataUpdate', (newUpdatedPlayerData)=>{
        const newPlayerData = JSON.parse(newUpdatedPlayerData)

        if(this.playerList !== newPlayerData) {
          newPlayerData.map((player, index) => {
            if(player.socketId != socket.id && this.playerList.find( listedPlayer => listedPlayer.socketId === player.socketId)) {
              const index = this.playerList.findIndex(oldPlayer => oldPlayer.socketId === player.socketId)
              this.playerList[index].setPosition(player.x, player.y)
              this.playerList[index].setRotation(player.rotation)
              this.playerList[index].stats.health = player.stats.health

            } else if(player.socketId != socket.id) {
              let newPlayer = {}
              newPlayer = this.add.container(300, 300)
              newPlayer.socketId = player.socketId
              newPlayer.anchor = 0.5
              newPlayer.setSize(64,64)
              newPlayer.collisionBoxData = this.cache.json.get('shipCollision')
              newPlayer.collisionBox = this.matter.add.gameObject(newPlayer, {isSensor: true, shape: newPlayer.collisionBoxData.ship})
              const playerTexture = this.add.sprite(0, 0, 'player')
              const playerCannonLeft = this.add.sprite(-10, -30, 'cannon')
              const playerCannonRight = this.add.sprite(-10, 30, 'cannon')
              playerCannonRight.scaleY = -1
              newPlayer.add(playerCannonLeft)
              newPlayer.add(playerCannonRight)
              newPlayer.add(playerTexture)
              newPlayer.setPosition(player.x, player.y)
              newPlayer.setRotation(player.rotation)
              newPlayer.stats = player.stats
              this.playerList.push(newPlayer)
            }
          })
        }
      })

      socket.on('playerDisconnected', disconnectedPlayerId => {
        const parsedDCData = JSON.parse(disconnectedPlayerId)
        parsedDCData.map( player => {
          const playerIndex = this.playerList.findIndex( oldPlayer => oldPlayer.socketId === player.socketId)
          if(this.playerList[playerIndex]) {
            this.playerList[playerIndex].destroy()
          }
          this.playerList.splice(playerIndex-1, 1)
        })
      })

      socket.on('newCannonShot', (cannonShotPosition) => {
        const newCannonShot = JSON.parse(cannonShotPosition)
        if(newCannonShot.socketId !== socket.id) {
          let bullet = this.bullets.get().setActive(true).setVisible(true)
          if (bullet) {
            const direction = newCannonShot.direction
            bullet = this.matter.add.gameObject(bullet, {isSensor: true, shape: { type: 'circle', radius: 8 } })
            bullet.fire(newCannonShot, direction);
            this.player.customSounds.cannonShot.play()
            bullet.socketId = newCannonShot.socketId
          }
        }
      })

      socket.on('hitRecieved', (newHitData) => {
        const hitData = JSON.parse(newHitData)
        if(hitData.playerHit === socket.id) {
          this.player.stats.health = this.player.stats.health - hitData.damageDealt
          this.game.events.emit('damageTaken', hitData.damageDealt)

          if(this.player.stats.health < 0) {
            console.log('Player ship drowned!')
            this.player.stats.health = 1000
            this.player.setPosition(500, 500)
          }
        }
      })


      this.player.customSounds = {
        cannonShot: this.sound.add('cannonShot', {volume: 0.3}),
        cannonExplosion: this.sound.add('cannonExplosion', {volume: 0.05}),
        emptyWaterSplash: this.sound.add('emptyWaterSplash', {volume: 0.1}),
      }

      this.mainCamera = this.cameras.main
      this.mainCamera.setBounds(0, 0, this.layer.width, this.layer.height)
      this.mainCamera.startFollow(this.player)

      this.controls = controlCreation(this.input)

      this.input.on('pointerdown', (pointer)=>{
        if(pointer.rightButtonDown()) {
          if(this.player.stats.rightBroadSide.stamina > this.player.stats.shotExhaustion) {
            this.player.stats.rightBroadSide.stamina = staminaHandler(this.game, this.player.stats.rightBroadSide, {side: 'right', ammount: this.player.stats.shotExhaustion, remove: true})
            let bullet = this.bullets.get().setActive(true).setVisible(true)
            if (bullet)
            {
              const direction = this.player.angle + 90
              const leftCannonPosition = this.player.list[1].getWorldTransformMatrix()
              const cannonPosition = {
                socketId: socket.id,
                x: leftCannonPosition.tx,
                y: leftCannonPosition.ty,
                rotation: this.player.rotation,
                direction: direction
              }
              bullet = this.matter.add.gameObject(bullet, {isSensor: true, shape: { type: 'circle', radius: 8 } })
              bullet.fire(cannonPosition, direction)
              socket.emit('cannonShot', JSON.stringify(cannonPosition))
              this.player.customSounds.cannonShot.play()
              bullet.socketId = socket.id
            }
          }
        }
        if(pointer.leftButtonDown()){
          if(this.player.stats.leftBroadside.stamina > this.player.stats.shotExhaustion) {
            this.player.stats.leftBroadside.stamina = staminaHandler(this.game, this.player.stats.leftBroadside, {side: 'left', ammount: this.player.stats.shotExhaustion, remove: true})

            let bullet = this.bullets.get().setActive(true).setVisible(true)
            if (bullet)
            {
              const direction = this.player.angle - 90
              const leftCannonPosition = this.player.list[0].getWorldTransformMatrix()
              const cannonPosition = {
                socketId: socket.id,
                x: leftCannonPosition.tx,
                y: leftCannonPosition.ty,
                rotation: this.player.rotation,
                direction: direction,
                shotDamage: this.player.stats.shotDamage
              }
              bullet = this.matter.add.gameObject(bullet, {isSensor: true, shape: { type: 'circle', radius: 8 } });
              bullet.fire(cannonPosition, direction);
              socket.emit('cannonShot', JSON.stringify(cannonPosition))
              this.player.customSounds.cannonShot.play()
              bullet.socketId = socket.id
            }
          }
        }
      })

      this.scale.on('resize', function(gameSize, baseSize, displaySize, resolution) {
        if(gameSize.width > 800 && gameSize.width < 1920 && gameSize.height > 600 && gameSize.height < 1080) {
          this.mainCamera.width = gameSize.width
          this.mainCamera.height = gameSize.height
        }
      }.bind(this));

      this.matter.world.on('collisionstart', function (event, bodyA, bodyB) {
        if(bodyB.gameObject && bodyB.gameObject.customType === "Bullet") {
          if(bodyA.gameObject && bodyA.gameObject.stats && bodyA.gameObject.socketId !== socket.id && bodyB.gameObject.socketId === socket.id) {
            console.log('Damaged enemy')
            bodyA.gameObject.stats.health-= 50
            socket.emit('playerHit', JSON.stringify({
              playerHit: bodyA.gameObject.socketId,
              damageDealt: this.player.stats.shotDamage
            }))
          }

          new ExplosionAnim(this, bodyB)
          this.player.customSounds.cannonExplosion.play()
          bodyB.gameObject.destroy()
        }
      }.bind(this))

      this.game.events.emit('updateMinStaminaStep', this.player.stats.shotExhaustion)
    },

    update: function (timestep, delta) {
        this.matter.world.step()
        movementHandler(this.controls, this.player, this.layer)
        this.player.stats.leftBroadside.stamina = staminaHandler(this.game, this.player.stats.leftBroadside, {side: 'left', ammount: this.player.stats.broadSideRegen, add: true})
        this.player.stats.rightBroadSide.stamina = staminaHandler(this.game, this.player.stats.rightBroadSide, {side: 'right', ammount: this.player.stats.broadSideRegen, add: true})
    }
});

export default GameScreen