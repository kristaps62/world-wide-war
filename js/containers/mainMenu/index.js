import React, { Component } from "react"
import ReactDOM from "react-dom"
import styles from './style.css'
import { handleServerRequest, getPlayerEquipment } from '../../components/utils/utils.js'
import Phaser from 'phaser'
import UIOverlay from '../UIOverlay'
import MainGame from '../mainGameScreen'
class MainMenu extends Component {
    constructor() {
      super();
      this.state = {
        uiOpen: true
      }
    }

    componentDidMount() {
      var config = {
          type: Phaser.AUTO,
          parent: 'phaser-container',
          scale: {
            mode: Phaser.Scale.RESIZE,
            autoCenter: Phaser.Scale.CENTER_BOTH
          },
          width: window.innerWidth,
          height: window.innerHeight,
          physics: {
            default: 'matter',
            matter: {
                gravity: {
                    y: 0
                },
                debug: true
            }
          },
          scene: [MainGame, UIOverlay]
      };

      const game = new Phaser.Game(config);
    }

    render() {
        const serverLogout = () => {
          function onLogoutComplete(data) {
            if(data) {
              localStorage.removeItem('exilelvLoginTokenStorage')
              this.props.changeLoginToken(false)
            } else {
              console.log('Logout was unsuccessful')
            }
          }

          if(localStorage && localStorage.getItem('exilelvLoginTokenStorage')) {
            const token = localStorage.getItem('exilelvLoginTokenStorage')
            handleServerRequest({sessionToken: token}, 'logout',onLogoutComplete.bind(this))
          }
        }

        const startGame = () => {
          this.setState({uiOpen: false})
        }

        return (
            <div className={styles.wrapper}>
                <div className={this.state.uiOpen ? styles.uiContainer : styles.uiContainerClosed}>
                <div className={styles.logoutButton} onClick={serverLogout}>Logout</div>
                Welcome!
                <div className={styles.startGameButton} onClick={startGame}>Start game</div>
                </div>
                <div id="phaser-container" className={styles.phaserContainer}></div>
            </div>
        )
    }
  }
  export default MainMenu;
