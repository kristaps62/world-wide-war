var path = require('path');
var JavaScriptObfuscator = require('webpack-obfuscator')

module.exports = {
  mode: 'production',
  entry: './index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.css$/,
        use: [
            'style-loader',
            'css-loader?modules&camelCase'
        ]
      }
    ]
  },
  plugins: [
      new JavaScriptObfuscator ({
        rotateUnicodeArray: true
    }, ['secretBundle.js'])
  ],
  watch: false
};
