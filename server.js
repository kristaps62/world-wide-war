const port = process.env.PORT || 1337;
const app = require('express')();
const bodyParser = require('body-parser')
app.use(function(req, res, next) {
      res.header("Access-Control-Allow-Credentials", "true")
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
      next();
    });
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

const server = app.listen(port);
const io = require('socket.io').listen(server);
const fs = require('fs')
const uuid = require('uuid/v1')
const executeMysqlQuery = require('./server-js/mysql/executeMysqlQuery')
const createAccount = require('./server-js/account/register/createAccount')
const checkIfAccountInfoIsValid = require('./server-js/account/validation/checkIfAccountInfoIsValid')
const checkIfAccountAlreadyExists = require('./server-js/account/validation/checkIfAccountAlreadyExists')
const userLoggedIn = require('./server-js/account/login/loginUser')
const logout = require('./server-js/account/logout')
const generateWorldTreasure = require('./server-js/treasureData/treasure')
const createTableIfNoneExists = "CREATE TABLE `exilelv_www`.`accounts` ( `id` INT(64) NOT NULL AUTO_INCREMENT , `username` VARCHAR(64) NOT NULL , `email` VARCHAR(64) NOT NULL , `password` VARCHAR(64) NOT NULL , `session` VARCHAR(64) NOT NULL , `session_id` VARCHAR(64) NOT NULL , `admin_level` INT(64) NOT NULL , PRIMARY KEY (`id`))"

//executeMysqlQuery(createTableIfNoneExists, "Created a mysql table for accounts if there wasn't one")

const worldTreasureData = generateWorldTreasure()

const playerList = []

io.on('connection', (socket) => {
  console.log('New player connected:'+socket.id)

  socket.on('newPlayerReady', (newPlayer) => {
    playerList.push(newPlayer)
    io.to(socket.id).emit('worldTreasureData', JSON.stringify(worldTreasureData))
  })

  socket.on('playerUpdated', (playerUpdatedData) => {
    const newPlayerData = JSON.parse(playerUpdatedData)

    let oldPlayerData = playerList.find(oldPlayer => oldPlayer.socketId === newPlayerData.socketId)

    if(oldPlayerData) {
      oldPlayerData = newPlayerData
      const index = playerList.findIndex(oldPlayer => oldPlayer.socketId === newPlayerData.socketId)
      playerList[index] = oldPlayerData

    }
  })

  socket.on('disconnect', () => {
    console.log('socket disconnected: '+ socket.id)
    console.log('Players left in server: '+ playerList.length)
    playerList.map( (player, index) => {
      if(player.socketId === socket.id) {
        const spliceAbleIndex = index--
        playerList.splice(spliceAbleIndex, 1)
      }
    })
    io.emit('playerDisconnected', JSON.stringify([{socketId: socket.id}]))
  })

  socket.on('cannonShot', (cannonShotPosition)=> {
    io.emit('newCannonShot', cannonShotPosition)
  })

  socket.on('playerHit', (hitData)=>{
    const newHitData = JSON.parse(hitData)
    console.log(newHitData)
    io.to(newHitData.playerHit).emit('hitRecieved', JSON.stringify(newHitData))
  })
})

setInterval(() => {
  io.emit('playerDataUpdate', JSON.stringify(playerList))
}, 25);

app.post('/api/login', (req, res)=>{
  userLoggedIn(req.body.email, req.body.password)
  .then((loggedInUserData)=>{
      if(loggedInUserData.isUserLoggedIn) {
        const userSessionId = uuid()
        executeMysqlQuery('UPDATE accounts SET session_id =`${userSessionId}` WHERE email = `${loggedInUserData.email}` ', '', ()=>{})
        res.send(JSON.stringify(userSessionId))
        console.log('User logged in with session id: '+userSessionId)
      } else {
        console.log('User failed to login')
        res.send('false')
    }
  })
})

app.post('/api/register', (req, res)=>{
  const accountData = req.body
  console.log('Account registration request recieved')
  console.log(accountData)
  if(checkIfAccountInfoIsValid(accountData)) {
    console.log('Account registration data correct moving to createAccount')
    createAccount(accountData.username, accountData.password, accountData.email, res)
  }
})


app.post('/api/logout', (req, res) => {
  logout(req.body.sessionToken, res)
})
